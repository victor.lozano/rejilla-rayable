[![pipeline status](https://gitlab.com/bardux/rejilla-rayable/badges/master/pipeline.svg)](https://gitlab.com/bardux/rejilla-rayable/commits/master)

# Rejilla rayable

Solamente es una rejilla que se puede colorear con el cursor

## Online
[https://bardux.gitlab.io/rejilla-rayable/](https://bardux.gitlab.io/rejilla-rayable/)